impress().init();

var start = true;

if (start) {
	$(".hint").delay(2000).fadeTo("slow", 1);
	$(window).on('hashchange', function(e){
		if ($(".hint").css('opacity')==0) $(".hint").remove();
			$(".hint").fadeTo("slow", 0, function() {
				$(".hint").remove();
			});
		});
	start=false;
}

$("#next").click(function(){impress().next();});
$("#prev").click(function(){impress().prev();});

var timeout = null;

$(document).on('mousemove', function() {
		
	$('#next').css({"transform":"translate(0px,0px)"});
	$('#prev').css({"transform":"translate(0px,0px)"});
	$('header').css({"transform":"translate(0px,0px)"});
	
	clearTimeout(timeout);

	timeout = setTimeout(function() {
		$('#next').css({"transform":"translate(250px,0px)"});
		$('#prev').css({"transform":"translate(-250px,0px)"});
		$('header').css({"transform":"translate(0px,-125px)"});
	}, 3000);
});